<?php
namespace DiaryTest;

use PDO;
use Diary\Model\DiaryModel;
use PHPUnit_Framework_TestCase;
use Fruit\Seed;
use Fruit\Config;

class DiaryModelTest extends PHPUnit_Framework_TestCase
{
    protected $re;

    public function testCreate()
    {
        $arr = array(
            "oday" => date("Y-m-d")
            );
        $arr2 = array(
            "oday" => date("Y-m-d"),
            "dtext" => "test"
            );
        $arr3 = array(
            "oday" => "2014-10-01",
            "dtext" => "test"
            );
        $this->assertEquals("資料錯誤或不完整", DiaryModel::create($arr)['detail']);
        $this->assertEquals("執行成功", DiaryModel::create($arr2)['detail']);
        $this->assertEquals("日記日期太遙遠或為未來日記", DiaryModel::create($arr3)['detail']);
    }

    public function testDelete()
    {
        $arr = array(
            "oday" => date("Y-m-d"),
            );
        $arr2 = array(
            "oday" => date("Y-m-d")
            );
        $arr3 = array(
            "dtext" => "test"
            );
        $this->assertEquals("執行成功", DiaryModel::delete($arr)['detail']);
        $this->assertEquals("沒有資料", DiaryModel::delete($arr2)['detail']);
        $this->assertEquals("資料錯誤或不完整", DiaryModel::delete($arr3)['detail']);
    }

    public function testUpdate()
    {
        $this->testCreate();
        $arr = array(
            "oday" => date("Y-m-d"),
            "sdtext" => "-test-"
            );
        $arr2 = array(
            "oday" => date("Y-m-d")
            );
        $arr3 = array(
            "oday" => "2014-10-01",
            "sdtext" => "test"
            );
        $this->assertEquals("執行成功", DiaryModel::update($arr)['detail']);
        $this->assertEquals("資料錯誤或不完整", DiaryModel::update($arr2)['detail']);
        $this->assertEquals("日記日期太遙遠或為未來日記", DiaryModel::update($arr3)['detail']);
        $this->assertEquals("沒有資料", DiaryModel::update($arr)['detail']);
    }

    public function testSelect()
    {
        $arr = array(
           "oday" => date("Y-m-d")
        );
        $result = DiaryModel::select($arr);
        //echo var_dump($result);
        $this->testDelete();
        $this->assertEquals("沒有資料", DiaryModel::select($arr)['detail']);
    }
}
