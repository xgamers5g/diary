<?php
namespace Diary\Lib;

class JsonCode
{
    /**
    * 送出json格式的資料
    * @param $arr 陣列
    * @return JSON格式的字串
    */
    public static function jsonencode($arr)
    {
        return json_encode($arr, JSON_UNESCAPED_UNICODE);
    }
}
