<?php
namespace DiaryTest;

use PDO;
use Diary\Model\DiaryModel;
use PHPUnit_Framework_TestCase;
use Fruit\Seed;
use Fruit\Config;
use Diary\Model\Response;
use Diary\Lib\JsonCode;

class DiaryControllerTest extends PHPUnit_Framework_TestCase
{
    protected $re;

    public function __construct()
    {
        $this->re = Response::responserule();
    }

    public function testCreate()
    {
        $arr = array(
            array(
                "day" => date("Y-m-d")
                ),
            array(
                "day" => date("Y-m-d"),
                "content" => "test"
                ),
            array(
                "day" => "2014-10-01",
                "content" => "test"
                ),
            array(
                "day" => date("Y-m-d"),
                "content" => "test"
                ),
            array(
                "day" => "abcde",
                "content" => "test"
                )
        );
        for ($i=0; $i<5; $i++) {
            $data = json_encode($arr[$i]);
            $ch = curl_init('http://Diary/create');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            switch ($i) {
                case '0':
                    $this->assertEquals(JsonCode::jsonencode($this->re['dataerror']), $result);
                    break;
                case '1':
                    $this->assertEquals(JsonCode::jsonencode($this->re['success']), $result);
                    break;
                case '2':
                    $this->assertEquals(JsonCode::jsonencode($this->re['datanotday']), $result);
                    break;
                case '3':
                    $this->assertEquals(JsonCode::jsonencode($this->re['datarepeat']), $result);
                    break;
                case '4':
                    $this->assertEquals(JsonCode::jsonencode($this->re['dataerror']), $result);
                    break;
                default:
                    echo "error";
                    break;
            }
        }
    }

    public function testDelete()
    {
        $arr = array(
            array(
                "content" => "test"
                ),
            array(
                "day" => date("Y-m-d")
                ),
            array(
                "day" => date("Y-m-d")
                )
        );
        for ($i=0; $i<3; $i++) {
            $data = json_encode($arr[$i]);
            $ch = curl_init('http://Diary/delete');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            switch ($i) {
                case '0':
                    $this->assertEquals(JsonCode::jsonencode($this->re['dataerror']), $result);
                    break;
                case '1':
                    $this->assertEquals(JsonCode::jsonencode($this->re['success']), $result);
                    break;
                case '2':
                    $this->assertEquals(JsonCode::jsonencode($this->re['datanot']), $result);
                    break;
                default:
                    echo "error";
                    break;
            }
        }
    }

    public function testUpdate()
    {
        $arr = array(
            array(
                "scontent" => "-test-"
                ),
            array(
                "day" => "2014-10-01",
                "scontent" => "-test-"
                ),
            array(
                "day" => date("Y-m-d"),
                "scontent" => "-test-"
                ),
            array(
                "day" => date("Y-m-d"),
                "scontent" => "-test-"
                )
        );
        for ($i=0; $i<4; $i++) {
            $data = json_encode($arr[$i]);
            $ch = curl_init('http://Diary/update');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            switch ($i) {
                case '0':
                    $this->assertEquals(JsonCode::jsonencode($this->re['dataerror']), $result);
                    break;
                case '1':
                    $this->assertEquals(JsonCode::jsonencode($this->re['datanotday']), $result);
                    break;
                case '2':
                    $this->assertEquals(JsonCode::jsonencode($this->re['datanot']), $result);
                    $this->testCreate();
                    break;
                case '3':
                    $this->assertEquals(JsonCode::jsonencode($this->re['success']), $result);
                    break;
                default:
                    echo "error";
                    break;
            }
        }
    }

    public function testSelect()
    {
        $arr = array(
           array(
            "day" => date("Y-m-d")
            ),
           array(
            "day" => "2014-10-01"
            )
        );
        for ($i=0; $i<2; $i++) {
            $data = json_encode($arr[$i]);
            $ch = curl_init('http://Diary/select');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            switch ($i) {
                case '0':
                    //echo $result;
                    break;
                case '1':
                    $this->assertEquals(JsonCode::jsonencode($this->re['datanot']), $result);
                    break;
                default:
                    echo "error";
                    break;
            }
        }
        $this->testDelete();
    }
}
