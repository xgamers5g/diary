<?php
ini_set("display_errors", "On");
error_reporting(E_ALL&~E_NOTICE);
require('init.php');
require(implode(DIRECTORY_SEPARATOR, [BASE_DIR, 'vendor', 'autoload.php']));
$cfg = new Fruit\Config(BASE_DIR);
Fruit\Seed::fertilize($cfg);
$cfg->getRouter()->route($_SERVER['DOCUMENT_URI']);
