<?php
namespace Diary\Model;

use PDO;
use DateTime;
use Diary\System\Model;
use Diary\Model\Response;

class DiaryModel extends Model
{
    /**
    * 取得DB連線
    */
    protected function __construct()
    {
        self::set();
    }

    /**
    * 新增此類別為物件的開口
    * @return new self()
    */
    public static function load()
    {
        return new self();
    }

    /**
    * 新增日記
    * @param $date 抓取目前的時間
    * @param $c cday 建立日期
    * @param $e eday 修改時間
    * @param $m7 抓取目前時間的前七天
    * @param $o oday 日記日期
    * @param $d dtext 日記內容
    * @return 回傳狀態
    */
    public static function create($arr)
    {
        $re = Response::responserule();
        $date = new DateTime('now');
        $c = $date->format('Y-m-d');
        $e = $date->format('Y-m-d H:i:s');
        $m7 = $date->modify('-7 day');
        if (isset($arr["oday"])) {
            if ($arr["oday"] > $c || $arr["oday"] < $m7->format('Y-m-d')) {
                return $re['datanotday'];
            } else {
                $o = $arr["oday"];
            }
        } else {
            $o = $c;
        }
        if (isset($arr["dtext"])) {
            $d = $arr["dtext"];
        } else {
            return $re['dataerror'];
        }
        Model::set();
        $result = null;
        $qs = "INSERT INTO diary (cday, oday, eday, dtext) VALUES (?, ?, ?, ?)";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $c, PDO::PARAM_STR);
        $stmt->bindValue(2, $o, PDO::PARAM_STR);
        $stmt->bindValue(3, $e, PDO::PARAM_STR);
        $stmt->bindValue(4, $d, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $stmt->closeCursor();
            $result = $re['success'];
            if ($stmt->rowCount() == 0) {
                $result = $re['datanot'];
            }
        } else {
            $result = $re['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 查詢日記
    * @param $o oday 日記日期
    * @return 成功回傳查詢資料，失敗回傳狀態
    */
    public static function select($arr)
    {
        $re = Response::responserule();
        $o = $arr["oday"];
        Model::set();
        $result = null;
        if ($arr == []) {
            $qs = "SELECT * FROM diary";
        } else {
            $qs = "SELECT * FROM diary WHERE oday = ?";
        }
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $o, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) == 0) {
                $result = $re['datanot'];
            }
        } else {
            $result = $re['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 修改日記
    * @param $date 抓取目前的時間
    * @param $se eday 修改時間
    * @param $m7 抓取目前時間的前七天
    * @param $o oday 日記日期
    * @param $so soday 修改後的日記日期    
    * @param $sd sdtext 修改後的日記內容
    * @return 回傳狀態
    */
    public static function update($arr)
    {
        $re = Response::responserule();
        if (!isset($arr["oday"]) || !isset($arr["sdtext"])) {
            return $re['dataerror'];
        }
        $date = new DateTime('now');
        $se = $date->format('Y-m-d H:i:s');
        $m7 = $date->modify('-7 day');
        if ($arr["oday"] > $se || $arr["oday"] < $m7->format('Y-m-d')) {
            return $re['datanotday'];
        }
        $o = $arr["oday"];
        if (isset($arr["soday"])) {
            $so = $arr["soday"];
        } else {
            $so = $o;
        }
        if (isset($arr["sdtext"])) {
            $sd = $arr["sdtext"];
        }

        Model::set();
        $result = null;
        $qs = "UPDATE diary SET oday = ?, eday = ?, dtext = ? WHERE oday = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $so, PDO::PARAM_STR);
        $stmt->bindValue(2, $se, PDO::PARAM_STR);
        $stmt->bindValue(3, $sd, PDO::PARAM_STR);
        $stmt->bindValue(4, $o, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $result = $re['success'];
            if ($stmt->rowCount() == 0) {
                $result = $re['datanot'];
            }
        } else {
            $result = $re['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }

    /**
    * 刪除日記
    * @param $o oday 日記日期
    * @return 成功回傳true，失敗回傳false
    */
    public static function delete($arr)
    {
        $re = Response::responserule();
        if (!isset($arr["oday"])) {
            return $re['dataerror'];
        }
        $o = $arr["oday"];
        Model::set();
        $result = null;
        $qs = "DELETE FROM diary WHERE oday = ?";
        $stmt = self::$db->prepare($qs);
        $stmt->bindValue(1, $o, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $result = $re['success'];
            if ($stmt->rowCount() == 0) {
                $result = $re['datanot'];
            }
        } else {
            $result = $re['databaseerror'];
        }
        $stmt->closeCursor();
        return $result;
    }
}
