<?php
require('init.php');
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']));
Fruit\Seed::Fertilize(new Fruit\Config(TEST_DIR));
