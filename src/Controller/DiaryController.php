<?php
namespace Diary\Controller;

use Diary\System\Controller;
use Diary\Controller\UserInput;
use Diary\Model\CheckInput;
use Diary\Model\DiaryModel;
use Diary\Model\Response;
use Diary\Lib\JsonCode;

class DiaryController extends Controller
{
    protected $uarr;
    protected $re;
    /**
     * 使用者輸入內容
     */
    public function __construct()
    {
        self::init();
        $arr = [
            'day' => self::$data->day,
            'sday' => self::$data->sday,
            'content' => self::$data->content,
            'scontent' => self::$data->scontent
        ];
        $this->uarr = CheckInput::userset($arr);
        $this->re = Response::responserule();
    }

    /**
    * 新增日記
    * @return 回傳狀態
    */
    public function create()
    {
        if (DiaryModel::select($this->uarr) == $this->re['datanot']) {
            return JsonCode::jsonencode(DiaryModel::create($this->uarr));
        } else {
            return JsonCode::jsonencode($this->re['datarepeat']);
        }
    }

    /**
    * 顯示日記
    * @return 成功回傳資料，失敗回傳狀態
    */
    public function select()
    {
        return JsonCode::jsonencode(DiaryModel::select($this->uarr));
    }

    /**
    * 修改日記
    * @return 回傳狀態
    */
    public function update()
    {
            return JsonCode::jsonencode(DiaryModel::update($this->uarr));
    }

    /**
    * 刪除日記
    * @return 回傳狀態
    */
    public function delete()
    {
        return JsonCode::jsonencode(DiaryModel::delete($this->uarr));
    }
}
