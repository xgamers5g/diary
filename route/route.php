<?php
//以下為route設定
require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']));

use Pux\Mux;

$mux = new Pux\Mux;


$mux->add(
    '/',
    ['Diary\Controller\MainController', 'index']
);

$mux->post('/select', ['Diary\Controller\DiaryController', 'select']);
$mux->post('/create', ['Diary\Controller\DiaryController', 'create']);
$mux->post('/update', ['Diary\Controller\DiaryController', 'update']);
$mux->post('/delete', ['Diary\Controller\DiaryController', 'delete']);

return $mux;
