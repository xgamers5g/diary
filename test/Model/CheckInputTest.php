<?php
namespace DiaryTest;

use PDO;
use PHPUnit_Framework_TestCase;
use Fruit\Seed;
use Fruit\Config;
use Diary\Model\CheckInput;
use Diary\Model\Response;
use Diary\Lib\JsonCode;

class CheckInputTest extends PHPUnit_Framework_TestCase
{
    protected $re;

    public function __construct()
    {
        $this->re = Response::responserule();
    }

    public function testcheckDate()
    {
        $date1 = "2014-10-20";
        $date2 = "2017";
        $date3 = "abcd";
        $this->assertEquals(true, CheckInput::checkDate($date1));
        $this->assertEquals($this->re['dataerror'], CheckInput::checkDate($date2));
        $this->assertEquals($this->re['dataerror'], CheckInput::checkDate($date3));
    }
}
