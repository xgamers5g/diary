<?php
namespace Diary\Model;

use Diary\Lib\Lib;
use DateTime;
use Exception;
use Diary\Model\Response;

class CheckInput
{
    /**
    * 判斷使用者輸入內容,格式正確才帶入陣列
    */
    public static function userset($arr)
    {
        $re = Response::responserule();
        $uarr = array();
        if (CheckInput::checkDate($arr["day"]) === true) {
            $uarr["oday"] = $arr["day"];
        } else {
            return CheckInput::checkDate($arr["day"]);
        }
        if (CheckInput::checkDate($arr["sday"]) === true) {
            $uarr["soday"] = $arr["sday"];
        }
        if (is_string($arr["content"])) {
            $uarr["dtext"] = $arr["content"];
        }
        if (is_string($arr["scontent"])) {
            $uarr["sdtext"] = $arr["scontent"];
        }
        return $uarr;
    }

    /**
    * 檢查時間是否符合格式
    * @param $date 為輸入的日期
    * @return 成功回傳true, 失敗回傳狀態
    */
    public static function checkDate($date)
    {
        $re = Response::responserule();
        try {
            $dt = new DateTime($date);
            $ck = $dt->format("Y-m-d");
            if ($ck == $date) {
                return true;
            } else {
                return $re['dataerror'];
            }
        } catch (Exception $e) {
            return $re['dataerror'];
        }
    }
}
